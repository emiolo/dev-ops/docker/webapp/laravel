#!/usr/bin/env bash
# arg1: name of image
# arg2: source type
# arg3: laravel version
# arg4: release version
# arg5: php version

IMAGE_NAME=$1
SOURCE=$2
LARAVEL_VERSION=${3:-5.5}
RELEASE=${4:-0.1}
PHP_VERSION=${5:-7.1}
SCRIPT_DIR=$(pwd)

tag_name="$IMAGE_NAME:$LARAVEL_VERSION-$SOURCE-php$PHP_VERSION-$RELEASE"
tag_latest="$IMAGE_NAME:$LARAVEL_VERSION-$SOURCE-php$PHP_VERSION"

cd "$LARAVEL_VERSION/$SOURCE/$RELEASE"

docker build \
  --tag $tag_name \
  --build-arg LARAVEL_VERSION=$LARAVEL_VERSION \
  --build-arg PHP_VERSION=$PHP_VERSION \
  --build-arg BASE_IMAGE \
  --build-arg BASE_TAG \
  .

docker tag $tag_name $tag_latest
docker push $tag_name
docker push $tag_latest

cd $SCRIPT_DIR
